/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */


#include "GameWindow.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "AssetEngine.h"
#include "SpriteEngine.h"
#include "config.h"
#include "debug.h"


GameWindow::GameWindow(int w, int h)
	:
	assetEngine(NULL),
	spriteEngine(NULL),
	window(NULL),
	renderer(NULL),
	width(w),
	height(h)
{
	window = SDL_CreateWindow("Lisa vs The Robots", 100, 100, width, height, SDL_WINDOW_SHOWN);

	if (window == NULL) {
		ERROR("Unable to create window: %s\n", SDL_GetError());
		exit(1);
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (renderer == NULL) {
		ERROR("Unable to create renderer: %s\n", SDL_GetError());
		exit(1);
	}

	assetEngine = new AssetEngine(renderer);
	spriteEngine = new SpriteEngine(renderer, assetEngine, window);
}


GameWindow::~GameWindow()
{
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}


void
GameWindow::CenterPosition(SDL_Texture* texture, SDL_Rect* position)
{
	if (!position) {
		ERROR("No position provided!\n");
		return;
	}

	// Center an image given width and height
	SDL_QueryTexture(texture, NULL, NULL, &position->w, &position->h);

	position->x = (width / 2) - (position->w / 2);
	position->y = (height / 2) - (position->h / 2);
}


void
GameWindow::ApplySurface(int x, int y, SDL_Texture* texture)
{
	SDL_Rect position;
	position.x = x;
	position.y = y;
	SDL_QueryTexture(texture, NULL, NULL, &position.w, &position.h);

	SDL_RenderCopy(renderer, texture, NULL, &position);
}


void
GameWindow::ShowSplash()
{
	// Load splash assets

	// Load a "close" background image to prevent scaling the image
	SDL_Texture* backgroundTexture;
	if (width <= 576)
		backgroundTexture = assetEngine->GetTexture("osdBackground576");
	else if (width <= 720)
		backgroundTexture = assetEngine->GetTexture("osdBackground720");
	else if (width <= 900)
		backgroundTexture = assetEngine->GetTexture("osdBackground900");
	else if (width <= 1080)
		backgroundTexture = assetEngine->GetTexture("osdBackground1080");
	else
		backgroundTexture = assetEngine->GetTexture("osdBackground1600");
		
	SDL_Texture* titleTexture = assetEngine->GetTexture("osdTitle");
	SDL_Texture* anykeyTexture = assetEngine->GetTexture("osdAnykey");

	// Find window center and center menuTitle
	SDL_Rect bgPosition;
	CenterPosition(backgroundTexture, &bgPosition);
	SDL_Rect titlePosition;
	CenterPosition(titleTexture, &titlePosition);

	// Fade in logo
	float alpha = 0;
	while (alpha < SDL_ALPHA_OPAQUE) {
		spriteEngine->RendererClear();
		ApplySurface(bgPosition.x, bgPosition.y, backgroundTexture);

		SDL_SetTextureAlphaMod(titleTexture, alpha);
		ApplySurface(titlePosition.x, titlePosition.y, titleTexture);
		spriteEngine->RendererPresent();
		alpha += 5;
	}
	SDL_Delay(1000);

	// Scroll logo to top
	int y = titlePosition.y;
	while (y >= 20) {
		spriteEngine->RendererClear();
		ApplySurface(bgPosition.x, bgPosition.y, backgroundTexture);

		SDL_SetTextureAlphaMod(titleTexture, alpha);
		ApplySurface(titlePosition.x, y, titleTexture);
		spriteEngine->RendererPresent();
		y -= 2;;
	}

	SDL_Rect anykeyPosition;
	CenterPosition(anykeyTexture, &anykeyPosition);
	ApplySurface(anykeyPosition.x, anykeyPosition.y + 100, anykeyTexture);
	SDL_RenderPresent(renderer);

	int exitSplash = 0;
	while (!exitSplash) {
		SDL_Event event;
		while(SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_KEYDOWN:
					exitSplash = 1;
					break;
				default:
					break;
			}
		}
	}
}


void
GameWindow::WalkTest()
{
	int exitGame = 0;
	while (!exitGame) {
		SDL_Event event;
		while(SDL_PollEvent(&event)) {
			switch (event.type) {
				case SDL_QUIT:
					exitGame = 1;
				case SDL_KEYDOWN:
					switch(event.key.keysym.sym) {
						case SDLK_RIGHT:
							spriteEngine->PlayerMove(PLAYER_MOVE_RIGHT);
							break;
						case SDLK_LEFT:
							spriteEngine->PlayerMove(PLAYER_MOVE_LEFT);
							break;
						case SDLK_SPACE:
							spriteEngine->PlayerAction(PLAYER_ACTION_JUMP);
							break;
					}
					break;
				case SDL_KEYUP:
					switch(event.key.keysym.sym) {
						case SDLK_RIGHT:
						case SDLK_LEFT:
							spriteEngine->PlayerMove(0);
							break;
					}
					break;
				default:
					break;
			}
		}

		// Draw frame
		spriteEngine->DrawBackground();
		//spriteEngine->DrawForground();
		spriteEngine->DrawPlayer();

		// Done drawing, present to window
		spriteEngine->RendererPresent();
	}
}
