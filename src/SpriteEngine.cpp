/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */


#include "SpriteEngine.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "AssetEngine.h"
#include "config.h"
#include "debug.h"


SpriteEngine::SpriteEngine(SDL_Renderer* activeRenderer, AssetEngine* assets, SDL_Window* gameWindow)
	:
	assetEngine(assets),
	renderer(activeRenderer),
	window(gameWindow)
{
	SDL_GetWindowSize(window, &windowWidth, &windowHeight);

	levelFloor = windowHeight - 120;

	playerSprite.position.x = 0;
	playerSprite.position.y = levelFloor;

	// Sync level position to player position
	levelPosition.y = playerSprite.position.y;
	levelPosition.x = playerSprite.position.x;

	playerState.action = 0;
	playerState.direction = PLAYER_MOVE_RIGHT;
	playerState.moving = 0;
}


void
SpriteEngine::RendererClear()
{
    SDL_RenderClear(renderer);
}


void
SpriteEngine::RendererPresent()
{
    SDL_RenderPresent(renderer);
}


void
SpriteEngine::RenderSprite(struct sprite_info* target)
{
	SDL_Texture* texture;

	long now = SDL_GetTicks();

	//TRACE("frameage: %ld\n", target->frameage);

	if (!target->frameage)
		target->frameage = now;

	if (target->animate) {
		//TRACE("frame: %d\n", target->frame);

		// Only get next frame if framerate has been surpased
		if (target->frameage + target->framerate <= now) {
			// TODO: oscillate
			target->frame++;
			target->frameage = now;
		}

		// On state changes, animation frame counts can differ
		if (target->frame > (target->framecount - 1)) {
			target->frameage = now;
			target->frame = 0;
		}

		char assetName[64];
		snprintf(assetName, 64, "%s%02d", target->assetBaseName, target->frame);
		texture = assetEngine->GetTexture(assetName);
	} else {
		target->frameage = now;
		texture = assetEngine->GetTexture(target->assetBaseName);
	}

	SDL_QueryTexture(texture, NULL, NULL, &target->position.w, &target->position.h);

	#if BOUNDING_BOX_ENABLE
	// Bounding box for debugging
	SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
	SDL_RenderDrawRect(renderer, &target->position);
	#endif

	SDL_RenderCopy(renderer, texture, NULL, &target->position);
}


void
SpriteEngine::ClearSprite(struct sprite_info* target)
{
	memset(target, 0, sizeof(target));
}


void
SpriteEngine::PlayerMove(int direction)
{
	playerState.moving = direction ? 1 : 0;

	TRACE("%s: move %d, moving %d\n", __func__, direction, playerState.moving);

	// If direction provided, update direction. Otherwise keep old direction.
	if (direction)
		playerState.direction = direction;
}


void
SpriteEngine::PlayerAction(int action)
{
	playerState.action = action;
}


void
SpriteEngine::MovementProcess()
{
	if (!playerState.moving)
		return;

	if (playerState.direction == PLAYER_MOVE_RIGHT) {
		levelPosition.x += 10;
		if (playerSprite.position.x <= (windowWidth / 4) * 3)
			playerSprite.position.x += 10;
	} else if (playerState.direction == PLAYER_MOVE_LEFT) {
		levelPosition.x -= 10;
		if (playerSprite.position.x >= (windowWidth / 4))
			playerSprite.position.x -= 10;
	}
		
}


void
SpriteEngine::ActionProcess()
{
	if (playerState.action == PLAYER_ACTION_JUMP) {
		if (playerSprite.position.y >= (levelFloor - 100)) {
			playerSprite.position.y -= 10;
			levelPosition.y -= 10;
		} else {
			playerState.action = 0;
		}
	} else {
		if (playerSprite.position.y <= levelFloor)
			levelPosition.y -= ApplyGravity(&playerSprite);
	}
}


void
SpriteEngine::DrawBackground()
{
	// Clear screen.
	// We draw background, foreground, and then player
	RendererClear();


	int y = 0;
	int step = 10;

	int r = 160;
	int g = 193;
	int b = 200;
	while (y < windowHeight) {
		SDL_Rect box;
		box.x = 0;
		box.y = y;
		box.w = windowWidth;
		box.h = step;

		SDL_SetRenderDrawColor(renderer, r, g, b, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(renderer, &box);

		r -= 2;
		b -= 2;
		g -= 2;
		if (r < 0)
			r = 0;
		if (g < 0)
			g = 0;
		if (b < 0)
			b = 0;

		y += step;
	}

	struct sprite_info sprite;
	ClearSprite(&sprite);
	sprite.position.y = 300;
	sprite.position.x = 0 - (levelPosition.x / 8);
	while (sprite.position.x < windowWidth) {
		strncpy(sprite.assetBaseName, "bgOutdoor", 64);
		RenderSprite(&sprite);
		sprite.position.x += 1024;
	}

	strncpy(sprite.assetBaseName, "tileCloud", 64);
	sprite.position.y = 10;
	sprite.position.x = (windowWidth - 512) - (levelPosition.x / 16);
	RenderSprite(&sprite);

	sprite.animate = 0;
	sprite.position.x = 0 - (levelPosition.x / 2);
	sprite.position.y = windowHeight - 48;

	int tile = 0;
	while (sprite.position.x < windowWidth) {
		if (tile > 4) {
			strncpy(sprite.assetBaseName, "tileFloorGrass01", 64);
			tile = 0;
		} else {
			strncpy(sprite.assetBaseName, "tileFloorGrass00", 64);
		}
		tile++;

		RenderSprite(&sprite);
		sprite.position.x += 48;
	}
}



void
SpriteEngine::DrawPlayer()
{
	playerSprite.animate = 1;
	
	switch (playerState.direction) {
		case PLAYER_MOVE_RIGHT:
			if (playerState.moving) {
				strncpy(playerSprite.assetBaseName, "walkRight", 64);
				playerSprite.framecount = 3;
				playerSprite.framerate = 100;
			} else {
				strncpy(playerSprite.assetBaseName, "waitRight", 64);
				playerSprite.framecount = 2;
				playerSprite.framerate = 2000;
			}
			break;
		case PLAYER_MOVE_LEFT:
			if (playerState.moving) {
				strncpy(playerSprite.assetBaseName, "walkLeft", 64);
				playerSprite.framecount = 3;
				playerSprite.framerate = 100;
			} else {
				strncpy(playerSprite.assetBaseName, "waitLeft", 64);
				playerSprite.framecount = 2;
				playerSprite.framerate = 2000;
			}
			break;
	}

	// Action images override walking / waiting ones
	switch (playerState.action) {
		case PLAYER_ACTION_JUMP:
			if (playerState.direction == PLAYER_MOVE_RIGHT) {
				strncpy(playerSprite.assetBaseName, "jumpRight", 64);
				playerSprite.framecount = 2;
				playerSprite.framerate = 500;
			} else if (playerState.direction == PLAYER_MOVE_LEFT) {
				strncpy(playerSprite.assetBaseName, "jumpLeft", 64);
				playerSprite.framecount = 2;
				playerSprite.framerate = 500;
			}
	}

	// Apply any needed action processing
	MovementProcess();
	ActionProcess();
	
	RenderSprite(&playerSprite);
}


int
SpriteEngine::ApplyGravity(struct sprite_info* target)
{
	int gravityReduction = 5;

	if (!target) {
		ERROR("%s: Invalid sprite!\n", __func__);
		return 0;
	}

	if (target->position.y < levelFloor) {
		target->position.y += gravityReduction;
	}

	return gravityReduction;
}


SpriteEngine::~SpriteEngine()
{
}
