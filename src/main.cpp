/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */


#include <SDL2/SDL.h>

#include "GameWindow.h"
#include "debug.h"


int
main(int argc, char** argv)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1) {
		ERROR("Unable to initialize SDL2: %s\n", SDL_GetError());
		return 1;
	}

	GameWindow* gameWindow = new GameWindow(1024, 576);
	
	gameWindow->ShowSplash();

	// A simple walk test
	gameWindow->WalkTest();

	SDL_Quit();
	return 0;
}
