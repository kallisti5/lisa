/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */


#include "AssetEngine.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "config.h"
#include "debug.h"


AssetEngine::AssetEngine(SDL_Renderer* activeRenderer)
	:
	renderer(activeRenderer)
{
	LoadImageAssets();
}


AssetEngine::~AssetEngine()
{
}


void
AssetEngine::LoadImageAssets()
{
	int successful = 0;
	int knownAssetCount = sizeof(kAssetInventory) / sizeof(kAssetInventory[0]);
	for (int i = 0; i < knownAssetCount; i++) {
		char path[4096];
		snprintf(path, 4096, "%s/%s", GAMEASSET_DIR, kAssetInventory[i].path);
	
		SDL_Surface* image = IMG_Load(path);
		if (!image) {
	        	ERROR("Unable to load game asset: %s\n", path);
				continue;
	 	}
		textures[i] = SDL_CreateTextureFromSurface(renderer, image);
		SDL_FreeSurface(image);
		if (!textures[i]) {
			ERROR("Unable to load game texture: %s\n", path);
			textures[i] = NULL;
		} else {
			successful++;
		}
	}
	TRACE("Loaded %d of %d image assets\n", successful, knownAssetCount);
}


SDL_Texture*
AssetEngine::GetTexture(const char* name)
{
	for (int i = 0; i < sizeof(kAssetInventory)
		/ sizeof(kAssetInventory[0]); i++) {

		if (strcmp(kAssetInventory[i].name, name) == 0)
			return textures[i];
	}
	ERROR("Asset '%s' not found!\n", name);
	return NULL;
}

