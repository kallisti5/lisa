#ifndef CONFIG_H
#define CONFIG_H

#define GAMEASSET_DIR "./assets"

// Debugging settings
#define DEBUG_TRACE_ENABLE	0
#define BOUNDING_BOX_ENABLE	0

#endif /* CONFIG_H */
