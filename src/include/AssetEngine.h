/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */
#ifndef __ASSETENGINE_H_
#define __ASSETENGINE_H_


#include <SDL2/SDL.h>


#define ASSETMAX 256


const struct asset_info {
	const char*	name;
	const char*	path;
} kAssetInventory[] = {
	{"osdTitle",			"png/osd/menuTitle.png"},
	{"osdBackground576",	"png/osd/background/1024x576-background.png"},
	{"osdBackground720",	"png/osd/background/1280x720-background.png"},
	{"osdBackground900",	"png/osd/background/1600x900-background.png"},
	{"osdBackground1080",	"png/osd/background/1920x1080-background.png"},
	{"osdBackground1600",	"png/osd/background/2844x1600-background.png"},
	{"bgOutdoor00",			"png/backgrounds/outdoor00.png"},
	{"osdAnykey",			"png/osd/anykey.png"},
	{"tileCloud00", 		"png/tiles/cloud00.png"},
	{"tileFloorMetal", 		"png/tiles/floor-metal.png"},
	{"tileFloorGrass00", 	"png/tiles/floor-grass00.png"},
	{"tileFloorGrass01", 	"png/tiles/floor-grass01.png"},
	{"walkLeft00",			"png/lisa/walk_left_00.png"},
	{"walkLeft01",			"png/lisa/walk_left_01.png"},
	{"walkLeft02",			"png/lisa/walk_left_02.png"},
	{"walkRight00",			"png/lisa/walk_right_00.png"},
	{"walkRight01",			"png/lisa/walk_right_01.png"},
	{"walkRight02",			"png/lisa/walk_right_02.png"},
	{"waitLeft00",			"png/lisa/wait_left_00.png"},
	{"waitLeft01",			"png/lisa/wait_left_01.png"},
	{"waitRight00",			"png/lisa/wait_right_00.png"},
	{"waitRight01",			"png/lisa/wait_right_01.png"},
	{"jumpLeft00",			"png/lisa/jump_left_00.png"},
	{"jumpLeft01",			"png/lisa/jump_left_01.png"},
	{"jumpRight00",			"png/lisa/jump_right_00.png"},
	{"jumpRight01",			"png/lisa/jump_right_01.png"},
};


class AssetEngine {
public:
							AssetEngine(SDL_Renderer* ActiveRenderer);
							~AssetEngine();
		void					LoadImageAssets();

		SDL_Texture*				GetTexture(const char* name);

private:
		SDL_Texture*				textures[ASSETMAX];
		SDL_Renderer*				renderer;
};

#endif /* __ASSETENGINE_H_ */
