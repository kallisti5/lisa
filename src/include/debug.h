/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */


#include <stdio.h>

#include "config.h"

#if DEBUG_TRACE_ENABLE
#define TRACE(x...) printf("TRACE: " x)
#else
#define TRACE(x...)
#endif

#define ERROR(x...) printf("ERROR: " x)
