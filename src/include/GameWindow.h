/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */
#ifndef __GAMEWINDOW_H_
#define __GAMEWINDOW_H_


#include <SDL2/SDL.h>

#include "AssetEngine.h"
#include "SpriteEngine.h"


class GameWindow {

public:
						GameWindow(int w, int h);
						~GameWindow();

		void			ShowSplash();
		void			WalkTest();

		SDL_Window* 	GetWindow() { return window; }
		SDL_Renderer* 	GetRenderer() { return renderer; }
		AssetEngine*	GetAssetEngine() { return assetEngine; }

		void			CenterPosition(SDL_Texture* texture, SDL_Rect* position);

		SDL_Renderer*	renderer;

private:

		void			ApplySurface(int x, int y, SDL_Texture* texture);

		AssetEngine*	assetEngine;
		SpriteEngine*	spriteEngine;
		SDL_Window*		window;
		int 			width;
		int 			height;
};

#endif /* __GAMEWINDOW_H_ */
