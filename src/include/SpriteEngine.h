/*
 * Lisa vs The Robots
 * 2013 - 2014, Alexander von Gluck IV
 *
 * Released under the terms of the GPLv2 license
 */
#ifndef __SPRITEENGINE_H_
#define __SPRITEENGINE_H_


#include <SDL2/SDL.h>

#include "AssetEngine.h"

#define MAX_FRAMES 				4

#define PLAYER_FRAME_MASK		0x000F

#define PLAYER_DIRECTION_MASK	0x00F0
#define PLAYER_DIRECTION_SHIFT	4
#define PLAYER_MOVE_RIGHT		0x1
#define PLAYER_MOVE_LEFT		0x2

#define PLAYER_ACTION_JUMP		0x1


struct player_state {
	int			direction;
	int			moving;
	int 			action;
};


struct sprite_info {
	// Asset information
	char 		assetBaseName[64];
	SDL_Rect 	position;

	// Animation settings (ignored if animated = 0)
	int 		animate;
	int 		frame;
	int 		framecount;
	int 		framerate; // Milliseconds
	long 		frameage;
	int 		oscillate;
};


class SpriteEngine {
public:
							SpriteEngine(SDL_Renderer* activeRenderer, AssetEngine* assets, SDL_Window* window);
							~SpriteEngine();

			void 			DrawBackground();
			void			DrawPlayer();

			void			PlayerMove(int direction);
			void			PlayerAction(int action);

			void 			RenderSprite(struct sprite_info* target);
			void			ClearSprite(struct sprite_info* target);

			void			ApplySurface(int x, int y, SDL_Texture* texture);
			int				ApplyGravity(struct sprite_info* target);
			void			RendererPresent();
			void			RendererClear();



private:
			void 			ActionProcess();
			void			MovementProcess();

			SDL_Window* 	window;
			int 			windowWidth;
			int 			windowHeight;

			int 			levelFloor;
			SDL_Rect		levelPosition;

			AssetEngine*	assetEngine;
			SDL_Renderer*	renderer;

			player_state	playerState;
	struct 	sprite_info 	playerSprite;
};


#endif /* __SPRITEENGINE_H_ */
