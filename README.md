# Lisa vs. the Robots

This is a thing I made long ago.
It's a platformer with a basic artifact management engine using some creative common graphics.

Enjoy the smooth(ha) scrolling parallax backgrounds and the professional(ha) graphics.

Maybe it'll serve to teach someone programming or something.

## Building

* Requires SDL

```scons```

## License

Released under the terms of the GPLv2. Copyright 2014-2019 Alexander von Gluck IV.

Some image assets are from a long-ago discontinued game from Joyride Laboratories GbR.
and released under a Creative Commons license. See LICENSE in the assets directory for
more information.
